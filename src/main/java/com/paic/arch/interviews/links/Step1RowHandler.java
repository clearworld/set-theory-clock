/**
 * 
 */
package com.paic.arch.interviews.links;

/**
 * @desc 第一排：每隔2s显示黄灯
 * @author mubreeze
 * @date 2018年3月7日
 */
public class Step1RowHandler extends RowHandler {

	public Step1RowHandler() {
		super.setHandler(new Step2RowHandler()); // 设置下一个步骤
	}

	@Override
	protected int getStep() {
		return 1;
	}

	@Override
	protected String getResult() {
		return super.convert(1 - (super.getSecond() % 2), 1, "Y").toString();
	}
}
