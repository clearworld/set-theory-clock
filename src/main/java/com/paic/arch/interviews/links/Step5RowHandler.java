/**
 * 
 */
package com.paic.arch.interviews.links;

/**
 * @desc 第五排处理:每盏灯代表1分钟
 * @author mubreeze
 * @date 2018年3月7日
 */
public class Step5RowHandler extends RowHandler {

	public Step5RowHandler() {

	}

	@Override
	protected int getStep() {
		return 5;
	}

	@Override
	protected String getResult() {
		return super.convert((super.getMinute() % 5), 4, "Y").toString();
	}
}
