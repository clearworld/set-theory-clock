/**
 * 
 */
package com.paic.arch.interviews.links;

/**
 * @desc 第二排处理:4盏灯，每盏灯代表5小时，显示红色
 * @author mubreeze
 * @date 2018年3月7日
 */
public class Step2RowHandler extends RowHandler {

	public Step2RowHandler() {
		super.setHandler(new Step3RowHandler());
	}

	@Override
	protected int getStep() {
		return 2;
	}

	@Override
	protected String getResult() {
		return super.convert((super.isIs24() ? 24 : (super.getHour() / 5)), 4, "R").toString();
	}
}
