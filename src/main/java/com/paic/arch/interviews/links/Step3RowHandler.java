/**
 * 
 */
package com.paic.arch.interviews.links;

/**
 * @desc 第3排处理:4盏灯，每盏灯代表1小时，显示红色
 * @author mubreeze
 * @date 2018年3月7日
 */
public class Step3RowHandler extends RowHandler {

	public Step3RowHandler() {
		super.setHandler(new Step4RowHandler());
	}

	@Override
	protected int getStep() {
		return 3;
	}

	@Override
	protected String getResult() {
		return super.convert((super.isIs24() ? 24 : (super.getHour() % 5)), 4, "R").toString();
	}
}
