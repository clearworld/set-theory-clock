/**
 * 
 */
package com.paic.arch.interviews.links;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

/**
 * @desc 处理器
 * @author mubreeze
 * @date 2018年3月7日
 */
public abstract class RowHandler {

	// the next step
	private RowHandler nextHandler;

	private int hour = 0;

	private int minute = 0;

	private int second = 0;

	private boolean is24 = false; // 是否是24点

	public final void execute(int step, String time, LinkedList<String> list) {
		setTime(time);
		if (this.getStep() == step) {
			list.add(this.getResult());
		}
		if (nextHandler != null) {
			nextHandler.execute(nextHandler.getStep(), time, list);
		}
	}

	public void setHandler(RowHandler handler) {
		this.nextHandler = handler;
	}

	protected abstract int getStep();

	/**
	 * 时间转换结果
	 * @return
	 */
	protected abstract String getResult();

	private void setTime(String time) {
		setIs24(time);
		Calendar calendar = getTime(time);
		this.setHour(calendar.get(Calendar.HOUR_OF_DAY));
		this.setMinute(calendar.get(Calendar.MINUTE));
		this.setSecond(calendar.get(Calendar.SECOND));
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public boolean isIs24() {
		return is24;
	}

	public void setIs24(String time) {
		this.is24 = time.startsWith("24");
	}

	/**
	 * 结果替换
	 * @param offset 替换截至索引
	 * @param length 结果总长度
	 * @param replace 替换字母
	 * @return
	 */
	public StringBuilder convert(int offset, int length, String replace) {
		StringBuilder builder = new StringBuilder();
		int index = 0;
		while (index < length) {
			if (index < offset) {
				builder.append(replace);
			} else {
				builder.append("O");
			}
			index++;
		}
		return builder;
	}

	/**
	 * 时间字符串转为时间类
	 * @param time
	 * @return
	 */
	public Calendar getTime(String time) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			Date date = formatter.parse(time);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			return calendar;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
}
