/**
 * 
 */
package com.paic.arch.interviews.links;

/**
 * @desc 第四排处理:11盏灯，每盏灯代表5分钟，其中第3、第6和第9盏的灯是红色，表示15分、30分和45分，其他的灯为黄色；
 * @author mubreeze
 * @date 2018年3月7日
 */
public class Step4RowHandler extends RowHandler {

	public Step4RowHandler() {
		super.setHandler(new Step5RowHandler());
	}

	@Override
	protected int getStep() {
		return 4;
	}

	@Override
	protected String getResult() {
		StringBuilder builder = super.convert((super.getMinute() / 5), 11, "Y");
		int mod = super.getMinute() / 15;
		int index = 1;
		while (mod > 0) {
			builder.replace(3 * index - 1, 3 * index, "R");
			mod--;
			index++;
		}
		return builder.toString();
	}
}
