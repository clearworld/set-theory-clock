/**
 * 
 */
package com.paic.arch.interviews;

import java.util.LinkedList;

import com.paic.arch.interviews.links.RowHandler;
import com.paic.arch.interviews.links.Step1RowHandler;

/**
 * @Desc 时间转换类<br>
 *       将5排时间转换结果分为5个步骤，从上至下<br>
 *       采用类责任链模式，在每一个步骤中设置好下一步骤<br>
 *       将每个步骤的时间转换结果存储到linkedList（记录其插入顺序，便于遍历结果）
 * @Author yehl
 * @Date 2018年3月7日
 */
public class TimeConverterImpl implements TimeConverter {

	private static TimeConverterImpl timeConverter;

	/**
	 * 获取单例
	 * @return
	 */
	public static TimeConverter getTimeConverter() {
		if (timeConverter == null) {
			timeConverter = new TimeConverterImpl();
		}
		return timeConverter;
	}

	private TimeConverterImpl() {
	}

	@Override
	public String convertTime(String time) {
		RowHandler handler = new Step1RowHandler();
		LinkedList<String> list = new LinkedList<>();
		handler.execute(1, time, list); // 从第一步开始执行，结果存list
		StringBuilder builder = new StringBuilder();
		int index = list.size();
		for (String string : list) {
			builder.append(string);
			if (index != 1) {
				builder.append("\r\n");
			}
			index--;
		}
		System.err.println(builder.toString());
		return builder.toString();
	}
}
